import axios from 'axios'

function LoginForm() {
    function loginSubmit() {
        axios.post('https://localhost:3000/auth');
    };
	
	return (
        <form onSubmit={loginSubmit}>
            <div>
                <label htmlFor='email'>Email: </label>
                <input type='email' id='email' />
            </div>
            <div>
                <label htmlFor='password'>Password: </label>
                <input type='password' id='password' />
            </div>
            <div>
                <label htmlFor='login'>Login: </label>
                <button id='login'>Login</button>
            </div>
        </form>
    );
}

export default LoginForm;
